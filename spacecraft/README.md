<Strong>Servicios REST disponibles</Strong>

/api/allSpacecraft{page,size} (GET) -> Este servicio devolverá todos los Spacecraft paginados.
/api/spacecrafts/{id} (GET) -> Este servicio devolverá el Spacecraft que tiene el id.
/api/spacecrafts{name} (GET) -> Este servicio requiere un nombre para buscar Spacecraft que contengan ese nombre.
/api/spacecrafts (POST) ->Esta petición requiere un Spacecraft que se creará
/api/spacecrafts (PUT) -> Esta petición requiere un Spacecraft que se actualizará
/api/spacecrafts/{id} (DELETE) -> Esta petición requiere el id de un spacecraft a borrar

BBDD

En este proyecto se tendrá una tabla (Spacecraft) usando un BBDD H2. Estos scripts son schema.sql donde se crean las tablas y data.sql donde se popula dicha BBDD.

Swagger

http://localhost:8080/swagger-ui/index.html

Para iniciar la app se puede hacer click derecho en SpacecraftApplication y hacer click en Run