package com.hiberus;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SpacecraftIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetSpacecrafts() throws Exception {
        mockMvc.perform(get("/api/allSpacecrafts?page=0&size=10"))
                .andExpect(status().isOk());
    }

}