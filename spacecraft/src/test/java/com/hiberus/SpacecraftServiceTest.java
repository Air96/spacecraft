package com.hiberus;

import com.hiberus.dto.SpacecraftDto;
import com.hiberus.model.Spacecraft;
import com.hiberus.repository.SpacecraftRepository;
import com.hiberus.service.SpacecraftServiceImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
@SpringBootTest
public class SpacecraftServiceTest {

    @Mock
    private SpacecraftRepository spacecraftRepository;

    @InjectMocks
    private SpacecraftServiceImp spacecraftServiceImp;

    @Test
    public void testGetSpacecraftById() {
        Spacecraft mockSpacecraft = new Spacecraft();
        mockSpacecraft.setId(1);
        mockSpacecraft.setName("Millennium Falcon");

        when(spacecraftRepository.findById(anyInt())).thenReturn(java.util.Optional.ofNullable(mockSpacecraft));

        SpacecraftDto result = spacecraftServiceImp.getSpacecraftById(1);

        verify(spacecraftRepository).findById(1);

        Assertions.assertNotNull(result);
        Assertions.assertEquals("Millennium Falcon", result.getName());
    }

    @Test
    public void testGetSpacecraftContainsName() {
        Spacecraft mockSpacecraft = new Spacecraft();
        mockSpacecraft.setName("Millennium Falcon");
        List<Spacecraft> spacecraftList =  Arrays.asList(mockSpacecraft);

        when(spacecraftRepository.findByNameContaining(anyString())).thenReturn(spacecraftList);

        List<SpacecraftDto> result = spacecraftServiceImp.getSpacecraftsByNameContains("Falcon");

        verify(spacecraftRepository).findByNameContaining("Falcon");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void testFindAllPaged() {
        List<Spacecraft> spacecraftList = Arrays.asList(
                new Spacecraft(1,"Millennium Falcon", "Star Wars",null),
                new Spacecraft(2,"USS Enterprise", "Star Trek",null),
                new Spacecraft(3,"Serenity", "Firefly",null)
        );

        Page<Spacecraft> spacecraftPage = new PageImpl(spacecraftList);
        when(spacecraftRepository.findAll(ArgumentMatchers.any(PageRequest.class))).thenReturn(spacecraftPage);

        Pageable pageable = PageRequest.of(0, 10);
        Page<SpacecraftDto> resultPage = spacecraftServiceImp.getAllSpacecrafts(pageable);

        verify(spacecraftRepository).findAll(PageRequest.of(0, 10));

        Assertions.assertNotNull(resultPage);
        Assertions.assertEquals(3, resultPage.getTotalElements());
        Assertions.assertEquals(3, resultPage.getContent().size());
    }

    @Test
    public void testCreate() {
        Spacecraft spacecraft = new Spacecraft();
        spacecraft.setName("Test Spacecraft");

        when(spacecraftRepository.save(ArgumentMatchers.any(Spacecraft.class))).thenReturn(spacecraft);

        SpacecraftDto spacecraftDto = new SpacecraftDto();
        spacecraftDto.setName("Test Spacecraft");
        SpacecraftDto savedSpacecraft = spacecraftServiceImp.createSpacecraft(spacecraftDto);

        verify(spacecraftRepository).save(spacecraft);
        Assertions.assertNotNull(savedSpacecraft);
        Assertions.assertEquals("Test Spacecraft", savedSpacecraft.getName());
    }

    @Test
    public void testDeleteById() {
        Spacecraft mockSpacecraft = new Spacecraft();
        mockSpacecraft.setId(1);
        mockSpacecraft.setName("Millennium Falcon");

        when(spacecraftRepository.findById(anyInt())).thenReturn(java.util.Optional.ofNullable(mockSpacecraft));
        Mockito.doNothing().when(spacecraftRepository).deleteById(1);

        boolean result = spacecraftServiceImp.deleteSpacecraft(1);

        Assertions.assertTrue(result);
        verify(spacecraftRepository, Mockito.times(1)).deleteById(1);
    }


}