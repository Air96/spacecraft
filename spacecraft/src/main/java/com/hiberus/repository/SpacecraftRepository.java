package com.hiberus.repository;

import com.hiberus.model.Spacecraft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpacecraftRepository extends JpaRepository<Spacecraft, Integer> {

    List<Spacecraft> findByNameContaining(String name);
}
