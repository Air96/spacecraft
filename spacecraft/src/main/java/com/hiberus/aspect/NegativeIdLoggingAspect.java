package com.hiberus.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NegativeIdLoggingAspect {
    private static final Logger logger = LoggerFactory.getLogger(NegativeIdLoggingAspect.class);

    @Before("execution(* com.hiberus.service.SpacecraftServiceImp.getSpacecraftById(Long)) && args(id)")
    public void logNegativeId(Long id) {
        if (id < 0) {
            logger.warn("Requested spacecraft with negative id: {}", id);
        }
    }
}