package com.hiberus.kafka.consumer;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;


@EnableKafka
@Configuration
public class KafkaConsumerSpacecraft {

    @KafkaListener(topics = "${kafka.topic}", groupId = "${kafka.group}")
    public void listenTopicActives(String message) {
            //Codigo correspondiente
    }

}
