package com.hiberus.kafka.producer;


import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.SendResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Configuration
public class KafkaProducerSpacecraft {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerSpacecraft.class);

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    //Usar este metodo desde la clase que se quiera mandar un mensaje
    public void sendMessage(String topic, String jsonStr) {
        CompletableFuture<SendResult<String, String>> future = kafkaTemplate().send(topic, jsonStr);
        future.whenComplete((result, ex) -> {
            if (ex == null) {
                LOGGER.info("sendMessage OK - topic [{}] - message [{}] - offset [{}]", topic, jsonStr, result.getRecordMetadata().offset());
            } else {
                LOGGER.error("sendMessage Error - topic [" + topic + "] - message [" + jsonStr + "] - ERROR: " + ex);
            }
        });
    }
}
