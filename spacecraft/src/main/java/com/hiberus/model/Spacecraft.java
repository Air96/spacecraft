package com.hiberus.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Entity
@Data
@Table(name="spacecraft")
@NoArgsConstructor
@AllArgsConstructor
public class Spacecraft implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="series")
    private String series;

    @Column(name="movie")
    private String movie;

}