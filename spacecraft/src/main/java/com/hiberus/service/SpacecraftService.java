package com.hiberus.service;


import com.hiberus.dto.SpacecraftDto;
import com.hiberus.model.Spacecraft;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface SpacecraftService {

    public Page<SpacecraftDto> getAllSpacecrafts(Pageable pageable);

    public SpacecraftDto getSpacecraftById(Integer id);

    public List<SpacecraftDto> getSpacecraftsByNameContains(String name);

    public SpacecraftDto createSpacecraft(SpacecraftDto spacecraftDto);

    public SpacecraftDto updateSpacecraft(Integer id, SpacecraftDto updatedSpacecraft);

    public boolean deleteSpacecraft(Integer id);
}
