package com.hiberus.service;


import com.hiberus.dto.SpacecraftDto;
import com.hiberus.model.Spacecraft;
import com.hiberus.repository.SpacecraftRepository;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SpacecraftServiceImp implements SpacecraftService{

    private static final Logger logger = LoggerFactory.getLogger(SpacecraftServiceImp.class);

    @Autowired
    private SpacecraftRepository spacecraftRepository;


    @Cacheable(value = "pagedSpacecrafts", key = "#pageable.pageNumber + '-' + #pageable.pageSize")
    public Page<SpacecraftDto> getAllSpacecrafts(Pageable pageable) {
        logger.info("Método getAllSpacecrafts invocado");
        Page<Spacecraft> spacecraftPage = spacecraftRepository.findAll(pageable);
        Page<SpacecraftDto> spacecraftDTOPage = spacecraftPage.map(this::convertToDTO);

        return spacecraftDTOPage;
    }

    public SpacecraftDto getSpacecraftById(Integer id) {
        return spacecraftRepository.findById(id).map(this::convertToDTO)
                .orElseThrow(() -> new EntityNotFoundException("Spacecraft not found with id: " + id));
    }

    public List<SpacecraftDto> getSpacecraftsByNameContains(String name) {
        return spacecraftRepository.findByNameContaining(name).stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    public SpacecraftDto createSpacecraft(SpacecraftDto spacecraftdto) {
        Spacecraft spacecraft = new Spacecraft();
        spacecraft.setName(spacecraftdto.getName());
        spacecraft.setSeries(spacecraftdto.getSeries());
        spacecraft.setMovie(spacecraftdto.getMovie());
        return convertToDTO(spacecraftRepository.save(spacecraft));
    }

    public SpacecraftDto updateSpacecraft(Integer id, SpacecraftDto updatedSpacecraft) {
        SpacecraftDto spacecraft = getSpacecraftById(id);
        spacecraft.setName(updatedSpacecraft.getName());
        spacecraft.setSeries(updatedSpacecraft.getSeries());
        spacecraft.setMovie(updatedSpacecraft.getMovie());
        return convertToDTO(spacecraftRepository.save(dtoToModel(spacecraft)));
    }


    @CacheEvict(value = "pagedSpacecrafts", allEntries = true)
    public boolean deleteSpacecraft(Integer id) {
        if (spacecraftRepository.findById(id).isPresent()) {
            spacecraftRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    private SpacecraftDto convertToDTO(Spacecraft spacecraft) {
        SpacecraftDto dto = new SpacecraftDto();
        dto.setId(spacecraft.getId());
        dto.setName(spacecraft.getName());
        dto.setMovie(spacecraft.getMovie());
        dto.setSeries(spacecraft.getSeries());
        return dto;
    }

    private Spacecraft dtoToModel(SpacecraftDto spacecraftDto) {
        Spacecraft model = new Spacecraft();
        model.setName(spacecraftDto.getName());
        model.setMovie(spacecraftDto.getMovie());
        model.setSeries(spacecraftDto.getSeries());
        return model;
    }
}
