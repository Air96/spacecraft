package com.hiberus.controller;

import com.hiberus.dto.SpacecraftDto;
import com.hiberus.exception.CustomException;
import com.hiberus.service.SpacecraftServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@Tag(name = "Spacecrafts", description = "The spacecraft API")
@RestController
@RequestMapping("/api")
public class SpacecraftController {

    @Autowired
    private SpacecraftServiceImp spacecraftServiceImp;

    @Operation(description = "Find all Spacecrafts pageable")
    @GetMapping(value="/allSpacecrafts", params = {"page", "size"})
    public ResponseEntity<Page<SpacecraftDto>> getAllSpacecrafts(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<SpacecraftDto> spacecraftsPage = spacecraftServiceImp.getAllSpacecrafts(pageable);
        return ResponseEntity.ok().body(spacecraftsPage);
    }

    @Operation(description = "Find Spacecrafts by id")
    @GetMapping("/spacecrafts/{id}")
    public ResponseEntity<SpacecraftDto> getSpacecraftById(@PathVariable Integer id) {
        SpacecraftDto spacecraft = spacecraftServiceImp.getSpacecraftById(id);
        if (spacecraft == null) {
            throw new CustomException("La nave espacial con ID " + id + " no fue encontrada.");
        }
        return ResponseEntity.ok(spacecraft);
    }

    @Operation(description = "Find Spacecrafts by containing name")
    @GetMapping(value="/spacecrafts", params = "name")
    public ResponseEntity<List<SpacecraftDto>> getSpacecraftsByNameContains(@RequestParam String name) {
        return ResponseEntity.ok().body(spacecraftServiceImp.getSpacecraftsByNameContains(name));
    }

    @Operation(description = "Create a Spacecraft")
    @PostMapping("/spacecrafts")
    public ResponseEntity<SpacecraftDto> createSpacecraft(@RequestBody SpacecraftDto spacecraft) {
        return ResponseEntity.accepted().body(spacecraftServiceImp.createSpacecraft(spacecraft));
    }

    @Operation(description = "Update a Spacecraft")
    @PutMapping("/spacecrafts")
    public ResponseEntity<SpacecraftDto> updateSpacecraft(@RequestParam Integer id, @RequestBody SpacecraftDto updatedSpacecraft) {
        return ResponseEntity.ok().body(spacecraftServiceImp.updateSpacecraft(id, updatedSpacecraft));
    }

    @Operation(description = "Delete a Spacecraft by id")
    @DeleteMapping("/spacecrafts/{id}")
    public  ResponseEntity<Void> deleteSpacecraft(@PathVariable Integer id) {
        boolean deleteSuccessful = spacecraftServiceImp.deleteSpacecraft(id);
        if (deleteSuccessful) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
