
DROP TABLE IF EXISTS spacecraft;

CREATE TABLE spacecraft (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    movie VARCHAR(255),
    series VARCHAR(255)
);