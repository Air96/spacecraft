-- Script para insertar datos de ejemplo en la tabla Spacecraft
INSERT INTO spacecraft ( name, movie, series) VALUES ( 'Millennium Falcon', 'Star Wars', 'A New Hope');
INSERT INTO spacecraft ( name, movie, series) VALUES ( 'X-Wing', 'Star Trek', 'The Motion Picture');
INSERT INTO spacecraft ( name, movie, series) VALUES ( 'Serenity', 'Firefly', 'Serenity');
INSERT INTO spacecraft ( name, movie, series) VALUES ( 'Battlestar Galactica', 'Battlestar Galactica', 'Battlestar Galactica');
INSERT INTO spacecraft ( name, movie, series) VALUES ( 'TARDIS', 'Doctor Who', 'Doctor Who');